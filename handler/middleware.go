package handler

import (
	"context"
	"net/http"

	"gitlab.com/davidjmacdonald1/onemoji/db"
)

const CTX_SESSION = "session"

func isAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session := db.GetUserSession(r)
		if !session.Verify(r.Context()) {
			http.SetCookie(w, session.Cookie())
			redirect(w, r, "/auth/login")
			return
		}

		ctx := context.WithValue(r.Context(), CTX_SESSION, &session)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
