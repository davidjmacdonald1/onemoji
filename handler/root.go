package handler

import (
	"net/http"

	"gitlab.com/davidjmacdonald1/onemoji/view/page"
)

func ShowHome() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		render(w, r, page.Home())
	})
}
