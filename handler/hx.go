package handler

import (
	"errors"
	"log"
	"net/http"
	"time"

	"gitlab.com/davidjmacdonald1/onemoji/db"
	"gitlab.com/davidjmacdonald1/onemoji/view/tag"
)

func HandleHx(router *http.ServeMux) {
	hx := http.NewServeMux()
	hx.HandleFunc("GET /feeling", getFeeling)
	hx.HandleFunc("GET /statuses", getStatuses)

	Handle("/hx/", router, hx)
}

func getFeeling(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSession(r)
	if !session.Verify(r.Context()) {
		r.AddCookie(session.Cookie())
		render(w, r, tag.Feeling(false, 0))
		return
	}

	var time time.Time
	var emoji int
	sql := `SELECT emoji, time FROM statuses
			WHERE user_id = $1 ORDER BY time DESC LIMIT 1`
	err := db.DB.QueryRow(r.Context(), sql, session.UserId).Scan(&emoji, &time)
	if err != nil && !errors.Is(err, db.ErrNoRows) {
		log.Println("Error finding latest status:", err)
		render(w, r, tag.Feeling(true, 128078)) // 👎
		return
	}

	if !isToday(time) {
		emoji = 0
	}
	render(w, r, tag.Feeling(true, emoji))
}

func getStatuses(w http.ResponseWriter, r *http.Request) {
	sql := `SELECT u.username, s.emoji, s.time
			FROM users u JOIN statuses s ON u.id = s.user_id
			ORDER BY s.time DESC`
	rows, err := db.DB.Query(r.Context(), sql)
	if err != nil {
		log.Println("Error finding statuses:", err)
		render(w, r, tag.Statuses([]tag.Status{}))
		return
	}
	defer rows.Close()

	statuses := []tag.Status{}
	for rows.Next() {
		status := tag.Status{}
		err := rows.Scan(&status.Username, &status.Emoji, &status.Time)
		if err != nil {
			log.Println("Error scanning statuses:", err)
			render(w, r, tag.Statuses(statuses))
			return
		}
		statuses = append(statuses, status)
	}
	render(w, r, tag.Statuses(statuses))
}
