package handler

import (
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/a-h/templ"
)

func Handle(path string, router *http.ServeMux, h http.Handler) {
	prefix := strings.TrimRight(path, "/")
	router.Handle(path, http.StripPrefix(prefix, h))
}

func isToday(t time.Time) bool {
	loc, _ := time.LoadLocation("America/New_York")
	now := time.Now().In(loc)
	midnight := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, loc)
	return !t.Before(midnight)
}

func render(w http.ResponseWriter, r *http.Request, t templ.Component) {
	err := t.Render(r.Context(), w)
	if err != nil {
		log.Println("Rendering error: ", err)
	}
}

func redirect(w http.ResponseWriter, r *http.Request, url string) {
	if r.Header.Get("Hx-Request") == "true" {
		w.Header().Set("Hx-Redirect", url)
		w.WriteHeader(http.StatusSeeOther)
	} else {
		http.Redirect(w, r, url, http.StatusSeeOther)
	}
}
