package handler

import (
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/davidjmacdonald1/onemoji/db"
	"gitlab.com/davidjmacdonald1/onemoji/view/tag"
)

func HandleUser(router *http.ServeMux) {
	user := http.NewServeMux()
	user.HandleFunc("POST /status", makeStatus)

	Handle("/user/", router, isAuth(user))
}

func makeStatus(w http.ResponseWriter, r *http.Request) {
	session := r.Context().Value(CTX_SESSION).(*db.UserSession)

	err := r.ParseForm()
	if err != nil {
		render(w, r, tag.Error("Bad request"))
		return
	}

	emoji, ok := parseEmoji(r.FormValue("code"))
	if !ok {
		render(w, r, tag.Error("Bad emoji"))
		return
	}

	var lastPost time.Time
	sql := `SELECT time FROM statuses WHERE user_id = $1 ORDER BY time DESC LIMIT 1`
	err = db.DB.QueryRow(r.Context(), sql, session.UserId).Scan(&lastPost)
	firstStatus := errors.Is(err, db.ErrNoRows)
	if !firstStatus && err != nil {
		log.Println("Error getting latest status:", err)
		render(w, r, tag.Error("Error getting latest status"))
		return
	}
	if !firstStatus && isToday(lastPost) {
		render(w, r, tag.Error("Cannot post twice in one day"))
		return
	}

	sql = `INSERT INTO statuses (emoji, user_id) VALUES ($1, $2)`
	_, err = db.DB.Exec(r.Context(), sql, emoji, session.UserId)
	if err != nil {
		log.Println("Error creating status:", err)
		render(w, r, tag.Error("Error creating status"))
		return
	}

	redirect(w, r, "/")
}

func parseEmoji(code string) (e int, ok bool) {
	e64, err := strconv.ParseInt(code, 10, 32)
	if err != nil {
		return e, false
	}

	e = int(e64)
	return e, tag.EMOJI_RANGE_BOTTOM <= e && e <= tag.EMOJI_RANGE_TOP
}
