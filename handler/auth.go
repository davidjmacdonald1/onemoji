package handler

import (
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/jackc/pgx/v5/pgconn"
	"gitlab.com/davidjmacdonald1/onemoji/db"
	"gitlab.com/davidjmacdonald1/onemoji/view/page"
	"gitlab.com/davidjmacdonald1/onemoji/view/tag"
	"golang.org/x/crypto/bcrypt"
)

func HandleAuth(router *http.ServeMux) {
	auth := http.NewServeMux()
	auth.HandleFunc("GET /signup", showSignupPage)
	auth.HandleFunc("POST /signup", signupUser)
	auth.HandleFunc("GET /login", showLoginPage)
	auth.HandleFunc("POST /login", loginUser)
	auth.HandleFunc("POST /logout", logoutUser)

	Handle("/auth/", router, auth)
}

func showSignupPage(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSession(r)
	if session.Verify(r.Context()) {
		redirect(w, r, "/")
	} else {
		render(w, r, page.Signup())
	}
}

func signupUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		render(w, r, tag.Error("Bad form request"))
		return
	}
	username := r.FormValue("username")
	password := r.FormValue("password")
	confirmPassword := r.FormValue("confirm-password")

	msg := verifySignup(username, password, confirmPassword)
	if msg != "" {
		render(w, r, tag.Error(msg))
		return
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		log.Println("Error hashing password:", err)
		render(w, r, tag.Error("Error hashing password"))
		return
	}

	var id int
	var pgErr *pgconn.PgError
	sql := `INSERT INTO users (username, hash) VALUES ($1, $2) RETURNING id`
	err = db.DB.QueryRow(r.Context(), sql, username, hash).Scan(&id)
	if errors.As(err, &pgErr) && pgErr.Code == "23505" {
		render(w, r, tag.Error("Username already taken"))
		return
	}
	if err != nil {
		log.Println("Error creating user:", err)
		render(w, r, tag.Error("Error creating user"))
		return
	}

	session := db.NewUserSession(r.Context(), id, username)
	if session == nil {
		log.Println("Error creating session:", err)
		render(w, r, tag.Error("Error creating session"))
		return
	}

	http.SetCookie(w, session.Cookie())
	redirect(w, r, "/")
}

func verifySignup(username, password, confirmPassword string) string {
	if password != confirmPassword {
		return "Passwords do not match"
	}
	return verifyLogin(username, password)
}

func showLoginPage(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSession(r)
	if session.Verify(r.Context()) {
		redirect(w, r, "/")
	} else {
		render(w, r, page.Login())
	}
}

func verifyLogin(username, password string) string {
	if len(username) < 4 || len(username) > 32 {
		return "Username must be between 4 and 32 chars"
	}

	if strings.Contains(username, db.SESSION_COOKIE_SEP) {
		return "Username cannot contain " + db.SESSION_COOKIE_SEP
	}

	if len(password) < 10 || len(password) > 70 {
		return "Password must be between 10 and 70 chars"
	}

	return ""
}

func loginUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		render(w, r, tag.Error("Bad form request"))
		return
	}
	username := r.FormValue("username")
	password := r.FormValue("password")

	msg := verifyLogin(username, password)
	if msg != "" {
		render(w, r, tag.Error(msg))
		return
	}

	var id int
	var hash []byte
	sql := `SELECT id, hash FROM users WHERE username = $1`
	err = db.DB.QueryRow(r.Context(), sql, username).Scan(&id, &hash)
	if errors.Is(err, db.ErrNoRows) {
		redirect(w, r, "/auth/signup")
		return
	}
	if err != nil {
		log.Println("Error finding user:", err)
		render(w, r, tag.Error("Error finding user"))
		return
	}

	if bcrypt.CompareHashAndPassword(hash, []byte(password)) != nil {
		render(w, r, tag.Error("Incorrect password"))
		return
	}

	session := db.NewUserSession(r.Context(), id, username)
	if session == nil {
		log.Println("Error creating session:", err)
		render(w, r, tag.Error("Error creating session"))
		return
	}

	http.SetCookie(w, session.Cookie())
	redirect(w, r, "/")
}

func logoutUser(w http.ResponseWriter, r *http.Request) {
	session := db.GetUserSession(r)
	session.Delete(r.Context())
	http.SetCookie(w, session.Cookie())
	redirect(w, r, "/")
}
