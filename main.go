package main

import (
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/davidjmacdonald1/onemoji/db"
	"gitlab.com/davidjmacdonald1/onemoji/handler"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Warning - error loading env:", err)
	}

	db.MustConnect()

	router := http.NewServeMux()
	router.Handle("/{$}", handler.ShowHome())
	router.Handle("/", http.FileServer(http.Dir("./public")))

	handler.HandleAuth(router)
	handler.HandleUser(router)
	handler.HandleHx(router)

	port := ":" + os.Getenv("PORT")
	log.Println("Starting server on localhost" + port)
	log.Fatal(http.ListenAndServe(port, router))
}

/*
	GET / - get partial home page
	GET /auth/login - show login form
	POST /auth/login - login user
	GET /auth/signup - show signup form
	POST /auth/signup - signup user
	POST /auth/logout - logout user

	GET /hx/feeling - sends feeling status and toggles logout button
	GET /hx/statuses - sends user statuses
	POST /user/status - creates user status
*/
