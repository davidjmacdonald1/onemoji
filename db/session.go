package db

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const SESSION_COOKIE_NAME = "Session"
const SESSION_COOKIE_SEP = ":"

type UserSession struct {
	UserId    int
	Username  string
	Token     string
	expiresAt time.Time
}

func NewUserSession(ctx context.Context, id int, username string) *UserSession {
	buf := make([]byte, 16)
	_, err := rand.Read(buf)
	if err != nil {
		log.Println("Error creating session token:", err)
		return nil
	}

	token := base64.RawURLEncoding.EncodeToString(buf)
	expiresAt := time.Now().Add(time.Hour * 48)

	sql := `INSERT INTO sessions(token, expires_at, user_id) VALUES ($1, $2, $3)`
	_, err = DB.Exec(ctx, sql, token, expiresAt, id)
	if err != nil {
		log.Println("Error inserting session into db:", err)
		return nil
	}

	return &UserSession{
		UserId:    id,
		Username:  username,
		Token:     token,
		expiresAt: expiresAt,
	}
}

func GetUserSession(r *http.Request) UserSession {
	cookie, err := r.Cookie(SESSION_COOKIE_NAME)
	if err != nil {
		return UserSession{}
	}

	value := strings.Split(cookie.Value, SESSION_COOKIE_SEP)
	if len(value) < 3 {
		log.Println("Bad session cookie value format (bad actor)")
		return UserSession{}
	}

	token, username := value[0], value[1]
	userId, err := strconv.ParseInt(value[2], 10, 64)
	if err != nil {
		log.Println("Bad session cookie userId (bad actor):", err)
		return UserSession{}
	}

	return UserSession{
		UserId: int(userId),
		Username: username,
		Token: token,
		expiresAt: cookie.Expires,
	}
}

func (u *UserSession) Verify(ctx context.Context) (ok bool) {
	if u.UserId == 0 {
		return false
	}

	sql := `SELECT true
			FROM users u INNER JOIN sessions s ON u.id = s.user_id
			WHERE s.token = $1 AND u.id = $2 AND s.expires_at > NOW()`
	err := DB.QueryRow(ctx, sql, u.Token, u.UserId).Scan(&ok)
	if err != nil && !errors.Is(err, ErrNoRows) {
		log.Println("Session verification error:", err)
		return false
	}

	return ok
}

func (u *UserSession) Delete(ctx context.Context) {
	u.expiresAt = time.Time{}
	if u.UserId == 0 {
		return
	}

	sql := `DELETE FROM sessions WHERE user_id = $1 AND (expires_at < NOW() OR token = $2)`
	_, err := DB.Exec(ctx, sql, u.UserId, u.Token)
	if err != nil {
		log.Println("Session deletion error:", err)
	}
}

func (u *UserSession) Cookie() *http.Cookie {
	c := SESSION_COOKIE_SEP
	value := fmt.Sprintf("%s%s%s%s%d", u.Token, c, u.Username, c, u.UserId)
	return &http.Cookie{
		Name:     SESSION_COOKIE_NAME,
		Value:    value,
		Path:     "/",
		Expires:  u.expiresAt,
		Secure:   true,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
}
