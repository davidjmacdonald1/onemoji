package db

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

var ErrNoRows = pgx.ErrNoRows

var DB *pgxpool.Pool

func MustConnect() {
	fmt.Println("Connecting to db...")
	db, err := pgxpool.New(context.Background(), os.Getenv("DB_URL"))
	if err != nil {
		log.Fatal("Error connecting to db:", err)
	}
	DB = db

	fmt.Print("Migrating tables...")
	mustCreateUsersTable()
	mustCreateStatusesTable()
	mustCreateSessionsTable()
	fmt.Println()
}

func mustCreateUsersTable() {
	sql := `CREATE TABLE IF NOT EXISTS users (
		id SERIAL PRIMARY KEY,
		username VARCHAR(32) UNIQUE NOT NULL,
		hash VARCHAR(60) NOT NULL,
		joined_at TIMESTAMP DEFAULT NOW(),
		last_login TIMESTAMP DEFAULT NOW()
	)`
	mustCreateTable("users", sql)
}

func mustCreateStatusesTable() {
	sql := `CREATE TABLE IF NOT EXISTS statuses (
		id SERIAL PRIMARY KEY,
		emoji INT NOT NULL,
		time TIMESTAMP DEFAULT NOW(),
		user_id INT REFERENCES users(id)
	)`
	mustCreateTable("statuses", sql)
}

func mustCreateSessionsTable() {
	sql := `CREATE TABLE IF NOT EXISTS sessions (
		id SERIAL PRIMARY KEY,
		token VARCHAR(22) UNIQUE NOT NULL,
		expires_at TIMESTAMP NOT NULL,
		user_id INT REFERENCES users(id)
	)`
	mustCreateTable("sessions", sql)
}

func mustCreateTable(name, sql string) {
	fmt.Print(" ", name)
	_, err := DB.Exec(context.Background(), sql)
	if err != nil {
		log.Fatal("Error creating table:", err)
	}
}
