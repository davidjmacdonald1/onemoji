let emojiInput = {
	form: document.querySelector("#emoji-input-form"),
	submitButton: document.querySelector("#emoji-input-submit"),
	undoButton: document.querySelector("#emoji-input-undo"),
	inputButtons: document.querySelectorAll(".emoji-input.button"),
	selected: undefined,
	selectButton: function(button) {
		const className = "is-success";
		this.selected?.classList.remove(className);
		this.selected = button;
		this.selected?.classList.add(className);
	},
	select: function(button) {
		const code = button.getAttribute("data-code");
		this.selectButton(button);

		this.form.setAttribute("hx-vals", JSON.stringify({ code: code }));
		this.submitButton.disabled = false;
		this.undoButton.disabled = false;
	},
	undo: function() {
		this.selectButton(undefined);
		this.submitButton.disabled = true;
		this.undoButton.disabled = true;
	},
};

emojiInput.inputButtons.forEach(button => {
	button.addEventListener("click", () => {
		emojiInput.select(button);
	});
});

emojiInput.undoButton.addEventListener("click", () => { emojiInput.undo(); });
window.addEventListener("load", () => { emojiInput.undo(); });
