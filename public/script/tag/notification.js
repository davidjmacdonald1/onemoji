let notification = {
	setListeners: function() {
		document.querySelectorAll(".notification").forEach((notif) => {
			let button = notif.querySelector("button");
			button.addEventListener("click", () => {
				notif.remove();
			});
		});
	}
};

