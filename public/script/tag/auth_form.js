let authForm = {
	onSubmit: (form) => {
		let button = form.querySelector("button.hx-submit");
		button.disabled = true;
	},
	onComplete: (form) => {
		let button = form.querySelector("button.hx-submit");
		button.disabled = false;
	}
};
